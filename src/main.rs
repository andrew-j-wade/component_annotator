use std::cmp::{max, min};
use std::collections::hash_map;
use std::collections::HashMap;
use std::collections::HashSet;
use std::error::Error;
use std::fs;
use std::hash::Hash;
use std::io;
use std::io::{BufRead, Write};
use std::ops::{Add, Rem, Sub};
use std::path::PathBuf;
use std::str::FromStr;

use rand::distributions::{Distribution, Uniform};
use regex::Regex;
use structopt::StructOpt;

#[derive(StructOpt)]
/// Annotate the elementary components present in the spaceships in the rle provided.
///
/// Only Life-like rules are supported. As a special case, rules matching the pattern Life*
/// are handled by assuming odd states are the active state.
///
/// The output is lifeviewer <https://www.conwaylife.com/wiki/LifeViewer> compatible comments
/// to be added to the rle.
struct Opt {
    /// Spaceship velocity in ([x],[y])c/[p] format
    ///
    /// The sign and order of the [x] and [y] components must correspond to
    /// the spaceships in the rle passed on stdin.
    #[structopt(long)]
    velocity: Velocity,

    /// The minimum size in active cells of an elementary component
    #[structopt(long, default_value = "25")]
    min_component_size: usize,

    /// Don't require elementary components to be disjoint.
    #[structopt(long)]
    allow_overlap: bool,

    /// Input rle file
    #[structopt(parse(from_os_str))]
    input: Option<PathBuf>,
}

#[derive(Clone, Copy)]
struct Velocity(i32, i32, i32);

impl Velocity {
    fn reflect_if(&self, reflect: bool, position: Position) -> Position {
        if !reflect {
            position
        } else if self.0 == 0 {
            Position(-position.0, position.1, position.2)
        } else if self.1 == 0 {
            Position(position.0, -position.1, position.2)
        } else if self.0 == self.1 {
            Position(position.1, position.0, position.2)
        } else if self.0 == -self.1 {
            Position(-position.1, -position.0, position.2)
        } else {
            panic!() /* no reflection possible */
        }
    }

    fn symmetric(&self) -> bool {
        self.0 == 0 || self.1 == 0 || self.0 == self.1 || self.0 == -self.1
    }
}

impl FromStr for Velocity {
    type Err = Box<dyn Error + Send + Sync>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"\(\s*([-+0-9]+)\s*,\s*([-+0-9]+)\s*\)\s*c\s*/\s*(\d+)").unwrap();
        let cap = re
            .captures(s)
            .ok_or_else(|| "Velocity format is incorrect")?;
        Ok(Velocity(
            cap[1].parse::<i32>()?,
            cap[2].parse::<i32>()?,
            cap[3].parse::<i32>()?,
        ))
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
struct Position(i32, i32, i32);

impl Rem<Velocity> for Position {
    type Output = Self;

    fn rem(self, v: Velocity) -> Self::Output {
        let mut rv = self;

        while rv.2 < 0 {
            rv.0 += v.0;
            rv.1 += v.1;
            rv.2 += v.2;
        }

        while rv.2 >= v.2 {
            rv.0 -= v.0;
            rv.1 -= v.1;
            rv.2 -= v.2;
        }

        rv
    }
}

impl Add for Position {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0, self.1 + other.1, self.2 + other.2)
    }
}

impl Sub for Position {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0, self.1 - other.1, self.2 - other.2)
    }
}

impl Position {
    fn neighborhood(&self) -> impl Iterator<Item = Self> + '_ {
        (-1..=1)
            .flat_map(move |dx| (-1..=1).map(move |dy| Position(self.0 + dx, self.1 + dy, self.2)))
    }

    fn window(size: i32, time_size: i32) -> impl Iterator<Item = Position> {
        (-size..=size).flat_map(move |dx| {
            (-size..=size)
                .flat_map(move |dy| (-time_size..=time_size).map(move |dz| Position(dx, dy, dz)))
        })
    }
}

struct Rule {
    birth: u32,
    survival: u32,
}

impl FromStr for Rule {
    type Err = Box<dyn Error + Send + Sync>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let re = Regex::new(r"B?(\d+)/S?(\d+)").unwrap();

        if let Some(cap) = re.captures(s) {
            let mut birth = 0;
            let mut survival = 0;
            for c in cap[1].chars() {
                if let Some(m) = c.to_digit(10) {
                    birth |= 1 << m;
                };
            }
            for c in cap[2].chars() {
                if let Some(m) = c.to_digit(10) {
                    survival |= 1 << m;
                };
            }
            Ok(Rule { birth, survival })
        } else if s.starts_with("Life") {
            Ok(Rule {
                birth: 12,
                survival: 8,
            })
        } else {
            Err(From::from(format!("Unrecognized rule: {}", &s)))
        }
    }
}

struct Cell {
    state: u8,
    component_refs: HashSet<ComponentRef>,
}

impl Cell {
    fn new(state: u8) -> Cell {
        Cell {
            state,
            component_refs: HashSet::new(),
        }
    }

    fn alive(&self) -> bool {
        self.state & 1 != 0
    }
}

#[derive(Clone)]
struct Component {
    centers: HashSet<(bool, Position)>,
    cells: HashSet<Position>,
}

impl Component {
    fn new<T: Iterator<Item = Position>>(cells: T) -> Component {
        Component {
            centers: HashSet::new(),
            cells: cells.collect(),
        }
    }

    fn seeds(universe: &Universe, velocity: Velocity, window_size: i32) -> Vec<Component> {
        let mut seeds: HashMap<Box<[Position]>, Component> = HashMap::new();

        for pos in universe.keys() {
            let mut w = Vec::new();
            let mut reflected = Vec::new();

            for dp in Position::window(window_size, window_size) {
                let pos = (*pos + dp) % velocity;
                if universe.get(&pos).map(|c| c.alive()) == Some(true) {
                    w.push(dp % velocity);
                    if velocity.symmetric() {
                        reflected.push(velocity.reflect_if(true, dp) % velocity);
                    };
                };
            }
            w.sort();
            reflected.sort();

            if w == reflected {
                continue; // don't try and handle symmetric windows
            };

            if let hash_map::Entry::Occupied(mut entry) = seeds.entry(reflected.into_boxed_slice())
            {
                entry.get_mut().centers.insert((true, *pos));
            } else {
                match seeds.entry(w.into_boxed_slice()) {
                    hash_map::Entry::Occupied(ref mut entry) => entry.get_mut(),
                    hash_map::Entry::Vacant(entry) => {
                        let new_component = Component::new(entry.key().iter().cloned());
                        entry.insert(new_component)
                    }
                }
                .centers
                .insert((false, *pos));
            }
        }

        seeds.values().cloned().collect()
    }

    fn is_already_covered(&self, universe: &Universe, velocity: Velocity) -> bool {
        let mut component_counts: HashMap<usize, usize> = HashMap::new();
        let mut current_location_counts: HashMap<ComponentRef, usize> = HashMap::new();
        let mut current_location_components: HashSet<usize> = HashSet::new();
        let cell_count = self.cells.len();

        for (reflect, center_pos) in self.centers.iter() {
            current_location_counts.clear();
            for delta in self.cells.iter() {
                let pos = (*center_pos + velocity.reflect_if(*reflect, *delta)) % velocity;
                for cref in universe[&pos].component_refs.iter() {
                    *current_location_counts.entry(*cref).or_insert(0) += 1;
                }
            }
            current_location_components.clear();
            for (key, value) in current_location_counts.iter() {
                if *value == cell_count {
                    current_location_components.insert(key.component_id);
                };
            }
            for component_id in current_location_components.iter() {
                *component_counts.entry(*component_id).or_insert(0) += 1;
            }
        }

        let center_count = self.centers.len();

        for count in component_counts.values() {
            if *count == center_count {
                return true;
            };
        }

        false
    }

    fn grow(&mut self, universe: &Universe, velocity: Velocity) {
        let mut grow_from: Vec<Position> = self.cells.iter().cloned().collect();

        while let Some(cell) = grow_from.pop() {
            for window_center in cell.neighborhood() {
                let mut match_pattern = None;
                let mut add_window = true;

                for (reflect, center) in self.centers.iter() {
                    let live_cells = Position::window(1, 1)
                        .filter(|delta| {
                            universe
                                .get(
                                    &((*center
                                        + velocity.reflect_if(*reflect, window_center + *delta))
                                        % velocity),
                                )
                                .map(|c| c.alive())
                                == Some(true)
                        })
                        .collect::<HashSet<_>>();

                    if match_pattern.is_none() {
                        match_pattern = Some(live_cells)
                    } else if *match_pattern.as_ref().unwrap() != live_cells {
                        add_window = false;
                        break;
                    };
                }
                if add_window {
                    for delta in match_pattern.unwrap().iter() {
                        let cell = (window_center + *delta) % velocity;
                        if !self.cells.contains(&cell) {
                            self.cells.insert(cell);
                            grow_from.push(cell);
                        };
                    }
                };
            }
        }
    }

    fn add_refs(&self, universe: &mut Universe, velocity: Velocity, component_id: usize) {
        for (reflect, center) in self.centers.iter() {
            let component_ref = ComponentRef {
                center: (*reflect, *center),
                component_id,
            };
            for delta in self.cells.iter() {
                universe
                    .get_mut(&((*center + velocity.reflect_if(*reflect, *delta)) % velocity))
                    .unwrap()
                    .component_refs
                    .insert(component_ref);
            }
        }
    }

    fn remove_refs(&self, universe: &mut Universe, velocity: Velocity, component_id: usize) {
        for (reflect, center) in self.centers.iter() {
            let component_ref = ComponentRef {
                center: (*reflect, *center),
                component_id,
            };
            for delta in self.cells.iter() {
                universe
                    .get_mut(&((*center + velocity.reflect_if(*reflect, *delta)) % velocity))
                    .unwrap()
                    .component_refs
                    .remove(&component_ref);
            }
        }
    }

    fn polylines(&self, outp: &mut dyn Write, velocity: Velocity) -> Result<(), io::Error> {
        // We de-duplicate the polygons by putting them in a HashSet.
        // Duplicates can come from the reflections of symmetric components.
        let mut polygons: HashSet<Box<[(i32, i32)]>> = HashSet::new();

        for (reflect, center) in self.centers.iter() {
            let mut min_x = i32::max_value();
            let mut min_y = i32::max_value();
            let mut max_x = i32::min_value();
            let mut max_y = i32::min_value();
            let mut min_x_plus_y = i32::max_value();
            let mut min_x_minus_y = i32::max_value();
            let mut max_x_plus_y = i32::min_value();
            let mut max_x_minus_y = i32::min_value();
            let mut corners: Vec<(i32, i32)> = Vec::new();

            for delta in self.cells.iter() {
                let universe_cell = (*center + velocity.reflect_if(*reflect, *delta)) % velocity;
                let x = universe_cell.0 * velocity.2 - universe_cell.2 * velocity.0;
                let y = universe_cell.1 * velocity.2 - universe_cell.2 * velocity.1;
                min_x = min(min_x, x);
                max_x = max(max_x, x);
                min_y = min(min_y, y);
                max_y = max(max_y, y);
                min_x_plus_y = min(min_x_plus_y, x + y);
                max_x_plus_y = max(max_x_plus_y, x + y);
                min_x_minus_y = min(min_x_minus_y, x - y);
                max_x_minus_y = max(max_x_minus_y, x - y);
            }
            corners.push((min_x_plus_y - min_y, min_y));
            corners.push((max_x_minus_y + min_y, min_y));
            corners.push((max_x, -max_x_minus_y + max_x));
            corners.push((max_x, max_x_plus_y - max_x));
            corners.push((max_x_plus_y - max_y, max_y));
            corners.push((min_x_minus_y + max_y, max_y));
            corners.push((min_x, -min_x_minus_y + min_x));
            corners.push((min_x, min_x_plus_y - min_x));
            polygons.insert(corners.into_boxed_slice());
        }

        let v = velocity.2 as f32;
        let elide_diagonal_threshold = velocity.2 / 2;

        for corners in polygons {
            write!(outp, "#C [[ POLYLINE ")?;
            if corners[0].0 - corners[7].0 > elide_diagonal_threshold {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[0].0 as f32 / v - 0.625,
                    corners[0].1 as f32 / v - 0.625
                )?;
            } else {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[7].0 as f32 / v - 0.625,
                    corners[0].1 as f32 / v - 0.625
                )?;
            };
            if corners[2].0 - corners[1].0 > elide_diagonal_threshold {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[1].0 as f32 / v + 0.625,
                    corners[1].1 as f32 / v - 0.625
                )?;
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[2].0 as f32 / v + 0.625,
                    corners[2].1 as f32 / v - 0.625
                )?;
            } else {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[2].0 as f32 / v + 0.625,
                    corners[1].1 as f32 / v - 0.625
                )?;
            };
            if corners[3].0 - corners[4].0 > elide_diagonal_threshold {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[3].0 as f32 / v + 0.625,
                    corners[3].1 as f32 / v + 0.625
                )?;
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[4].0 as f32 / v + 0.625,
                    corners[4].1 as f32 / v + 0.625
                )?;
            } else {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[3].0 as f32 / v + 0.625,
                    corners[4].1 as f32 / v + 0.625
                )?;
            };
            if corners[5].0 - corners[6].0 > elide_diagonal_threshold {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[5].0 as f32 / v - 0.625,
                    corners[5].1 as f32 / v + 0.625
                )?;
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[6].0 as f32 / v - 0.625,
                    corners[6].1 as f32 / v + 0.625
                )?;
            } else {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[6].0 as f32 / v - 0.625,
                    corners[5].1 as f32 / v + 0.625
                )?;
            };
            if corners[0].0 - corners[7].0 > elide_diagonal_threshold {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[7].0 as f32 / v - 0.625,
                    corners[7].1 as f32 / v - 0.625
                )?;
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[0].0 as f32 / v - 0.625,
                    corners[0].1 as f32 / v - 0.625
                )?;
            } else {
                write!(
                    outp,
                    "{:.2} {:.2} ",
                    corners[7].0 as f32 / v - 0.625,
                    corners[0].1 as f32 / v - 0.625
                )?;
            };

            writeln!(outp, "4 ]]")?;
        }
        Ok(())
    }
}

#[derive(Eq, PartialEq, Hash, Clone, Copy)]
struct ComponentRef {
    center: (bool, Position),
    component_id: usize,
}

type Universe = HashMap<Position, Cell>;

fn evolve(universe: &mut Universe, gen: i32, rule: &Rule) {
    let positions: HashSet<Position> = universe
        .iter()
        .filter(|&x| (x.0).2 == gen && x.1.alive())
        .map(|x| *x.0)
        .collect();

    for probe in (&positions).iter().flat_map(|p| p.neighborhood()) {
        let cell_count = probe
            .neighborhood()
            .filter(|p| positions.contains(&p))
            .count();
        let next_alive = if positions.contains(&probe) {
            rule.survival & (1 << (cell_count - 1)) != 0
        } else {
            rule.birth & (1 << cell_count) != 0
        };
        if next_alive {
            universe.insert(Position(probe.0, probe.1, gen + 1), Cell::new(1));
        };
    }
}

fn load_universe(
    inp: &mut dyn BufRead,
) -> Result<(Universe, String), Box<dyn Error + Send + Sync>> {
    let mut universe = HashMap::new();
    let mut rule = String::new();
    let mut x = 0;
    let mut y = 0;

    let runs = Regex::new(r"(\d*)([p-y]?)([A-Z.ob$!])").unwrap();

    for line in inp.lines() {
        let line = line?;
        let line = line.trim();
        if line.starts_with('x') {
            let re = Regex::new(r"^x\s*=\s*\d+\s*,\s*y\s*=\s*\d+\s*,\s*rule\s*=\s*(\S+)").unwrap();
            for caps in re.captures_iter(&line) {
                rule = caps[1].into();
            }
        } else if line.starts_with('#') {
            // ignore
        } else {
            for caps in runs.captures_iter(&line) {
                let run_length = caps[1].parse::<i32>().unwrap_or(1);
                match &caps[3] {
                    "!" => return Ok((universe, rule)),
                    "$" => {
                        y += run_length;
                        x = 0
                    }
                    "b" | "." => {
                        x += run_length;
                    }
                    "o" => {
                        for x2 in x..(x + run_length) {
                            universe.insert(Position(x2, y, 0), Cell::new(1));
                        }
                        x += run_length;
                    }
                    c => {
                        let state = match &caps[2] {
                            "" => 0,
                            c2 => (c2.chars().next().unwrap() as u8 - b'o') * 24,
                        } + (c.chars().next().unwrap() as u8 - b'A')
                            + 1;
                        for x2 in x..(x + run_length) {
                            universe.insert(Position(x2, y, 0), Cell::new(state));
                        }
                        x += run_length;
                    }
                };
            }
        }
    }

    Ok((universe, rule))
}

fn grow_components(
    components: &mut Vec<Component>,
    universe: &mut Universe,
    velocity: Velocity,
    min_component_size: usize,
) {
    let mut results: Vec<Component> = Vec::new();

    for mut component in components.drain(..) {
        if !component.is_already_covered(&universe, velocity) {
            component.grow(&universe, velocity);
            if component.cells.len() >= min_component_size {
                component.add_refs(universe, velocity, results.len());
                results.push(component);
            };
        };
    }
    components.append(&mut results);
}

fn intersect_components(
    components: &mut Vec<Component>,
    universe: &mut Universe,
    velocity: Velocity,
    min_component_size: usize,
) {
    let mut next_component = 0;
    let mut other_ref;
    let mut cells = HashSet::new();
    let mut other_cells = HashSet::new();
    let mut center_transform = (false, Position(0, 0, 0));

    // Intersect components
    while next_component < components.len() {
        components[next_component].remove_refs(universe, velocity, next_component);
        loop {
            other_ref = None;
            for (reflect, center) in components[next_component].centers.iter() {
                for delta in components[next_component].cells.iter() {
                    let probe = (*center + velocity.reflect_if(*reflect, *delta)) % velocity;
                    let component_refs = &universe[&probe].component_refs;
                    if !component_refs.is_empty() {
                        other_ref = component_refs.iter().next().copied();
                        break;
                    };
                }
                if let Some(other_ref) = other_ref {
                    // find intersecting cells
                    cells.clear();
                    other_cells.clear();
                    for delta in components[next_component].cells.iter() {
                        let probe = (*center + velocity.reflect_if(*reflect, *delta)) % velocity;
                        if universe[&probe].component_refs.contains(&other_ref) {
                            cells.insert(*delta);
                            other_cells.insert(
                                velocity.reflect_if(other_ref.center.0, probe - other_ref.center.1)
                                    % velocity,
                            );
                        };
                    }
                    center_transform = (
                        *reflect == other_ref.center.0,
                        velocity.reflect_if(other_ref.center.0, *center - other_ref.center.1),
                    );

                    break;
                };
            }
            if let Some(other_ref) = other_ref {
                // We will be modifying the other component so we want to remove refs temporarily
                components[other_ref.component_id].remove_refs(
                    universe,
                    velocity,
                    other_ref.component_id,
                );
                components[next_component].cells = &components[next_component].cells - &cells;
                components[other_ref.component_id].cells =
                    &components[other_ref.component_id].cells - &other_cells;
                //add back the refs
                components[other_ref.component_id].add_refs(
                    universe,
                    velocity,
                    other_ref.component_id,
                );
                //now create the intersection component
                let mut component = Component::new(cells.iter().copied());
                component.centers = components[next_component].centers.clone();
                // And for the tricky part. The new component can appear in other_component
                // in places where next_component isn't. centers is a HashSet so will de-duplicate
                // centers as we add them.
                component
                    .centers
                    .extend(components[other_ref.component_id].centers.iter().map(
                        |(reflect, center)| {
                            (
                                *reflect == center_transform.0,
                                (*center + velocity.reflect_if(*reflect, center_transform.1))
                                    % velocity,
                            )
                        },
                    ));
                component.add_refs(universe, velocity, components.len());
                components.push(component);
            } else {
                break;
            };
        }

        //At this point the current component does not intersect any components.
        //But check if it has been split into islands.
        if !components[next_component].cells.is_empty() {
            let mut grow_from: Vec<Position> = Vec::new();
            let mut island: HashSet<Position> = HashSet::new();

            grow_from.push(*components[next_component].cells.iter().next().unwrap());
            island.insert(grow_from[0]);

            while let Some(cell) = grow_from.pop() {
                for delta in Position::window(2, 1) {
                    let probe = (cell + delta) % velocity;
                    if components[next_component].cells.contains(&probe) && !island.contains(&probe)
                    {
                        island.insert(probe);
                        grow_from.push(probe);
                    };
                }
            }
            if island != components[next_component].cells {
                // We have more than one island. Split into the current island and a new component
                // for the remaining cells
                let mut component = Component::new(
                    components[next_component]
                        .cells
                        .difference(&island)
                        .copied(),
                );
                component.centers = components[next_component].centers.clone();
                component.add_refs(universe, velocity, components.len());
                components.push(component);

                components[next_component].cells = island;
            };
        };

        next_component += 1;
    }

    components.retain(|c| c.cells.len() >= min_component_size);
}

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let opt = Opt::from_args();
    let velocity = opt.velocity;

    let stdin = io::stdin();
    let mut inp: Box<dyn BufRead> = match opt.input {
        Some(path) => Box::new(io::BufReader::new(fs::File::open(path)?)),
        None => Box::new(stdin.lock()),
    };
    let (mut universe, rule) = load_universe(&mut *inp)?;
    drop(inp);

    let rule = rule.parse::<Rule>()?;

    for g in 0..(opt.velocity.2 - 1) {
        evolve(&mut universe, g, &rule);
    }

    let mut components = Component::seeds(&universe, opt.velocity, 2);
    components.append(&mut Component::seeds(&universe, opt.velocity, 1));

    grow_components(
        &mut components,
        &mut universe,
        opt.velocity,
        opt.min_component_size,
    );
    intersect_components(
        &mut components,
        &mut universe,
        opt.velocity,
        opt.min_component_size,
    );

    // The first iteration can remove some components that become too fragmented, which
    // can allow larger components in the second iteration.
    grow_components(
        &mut components,
        &mut universe,
        opt.velocity,
        opt.min_component_size,
    );
    if !opt.allow_overlap {
        intersect_components(
            &mut components,
            &mut universe,
            opt.velocity,
            opt.min_component_size,
        );
    };

    let stdout = io::stdout();
    let mut outp = stdout.lock();

    writeln!(outp, "#C [[ POLYSIZE 1 ]]")?;
    for component in &mut components {
        // Pick a random color. To help out colorblind users, force a uniform distribution
        // in lightness. Blue is seen as darker so provide a boost if the color is blue.
        let blue_lightness = Uniform::from(350..3 * 255).sample(&mut rand::thread_rng());
        let lightness = Uniform::from(255..3 * 255).sample(&mut rand::thread_rng());
        let (r, g, b) = match Uniform::from(0..6).sample(&mut rand::thread_rng()) {
            0 => (255, (lightness - 255) / 2, (lightness - 255) / 2),
            1 => ((lightness - 255) / 2, 255, (lightness - 255) / 2),
            2 => ((blue_lightness - 255) / 2, (blue_lightness - 255) / 2, 255),
            3 => (
                max(0, lightness - 2 * 255),
                min(255, lightness / 2),
                min(255, lightness / 2),
            ),
            4 => (
                min(255, lightness / 2),
                max(0, lightness - 2 * 255),
                min(255, lightness / 2),
            ),
            5 => (
                min(255, lightness / 2),
                min(255, lightness / 2),
                max(0, lightness - 2 * 255),
            ),
            _ => {
                (lightness / 3, lightness / 3, lightness / 3) // never returned
            }
        };
        writeln!(outp, "#C [[ COLOR POLY {} {} {} ]]", r, g, b)?;
        component.polylines(&mut outp, velocity)?;
    }

    Ok(())
}
