# component_annotator: A component annotator for Conway life spaceships.

## INSTALLING

1. Install <https://www.rust-lang.org>

2. Clone the repository.

3. Run "cargo build --release"

This program was developed on linux but should work on all major operating
systems.

## USAGE

1. Run ./target/release/component_annotator (--help will provide instructions.)

2. Use the output annotations along with the input rle with LifeViewer
(<https://www.conwaylife.com/wiki/LifeViewer>)

## DISCUSSION

For the purposes of this program, elementary components:

* Are strictly disjoint in their active cells. When it is unclear which of two neighboring components a cell belongs to, it belongs to neither. (This plays the part of "not reducible to a combination of smaller parts", but is a somewhat stricter condition.)

* Have "reasonable" boundaries.

* Are of reasonable size (A single live cell is not a component.)

* Subject to the other constaints are the maximal size that does not reduce the number of contexts in which they are found.

Elementary components may be defined relative to the space of all spaceships, but as a practical matter can only be identified relative to the *known* spaceships and as such are subject to change as additional spaceships are discovered.

## ALGORITHM

The basic algorithm is:

1. Find maximal components without regard to being elementary.

2. For each pair of intersecting components, replace them with three components formed by the intersection, and the two asymmetric differences, of the two parent components.

3. Remove undersized fragments from consideration as components, and repeat the process. (The definition of elementary components is somewhat circular, and this refinement provides better results.)

## BUGS

* In general more spaceships will work better than fewer, but this can exceed
  the posting limit on the conwaylife.com forums.